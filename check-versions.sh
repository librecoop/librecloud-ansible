# script to check if the versions of images that are
# changed and not staged exist in the registries
# optionally use ./check-versions.sh --cached

# shellcheck disable=SC2086
images=$(git diff $1 roles/librecoop.deploy/defaults/main.yml \
| grep -E '^\+.+image_version' \
| sed -r 's|\+(.+)_image_version: "(.+)"|\1:\2|' \
| sed 's|backupbot:|registry.gitlab.com/librecoop/backup-bot-two:v|' \
| sed -r 's|blackbox:|quay.io/prometheus/blackbox-exporter:v|' \
| sed 's|cadvisor:|gcr.io/cadvisor/cadvisor:v|' \
| sed 's|clamav|clamav/clamav-debian|' \
| sed 's|elasticsearch|docker.elastic.co/elasticsearch/elasticsearch|' \
| sed 's|grafana|grafana/grafana|' \
| sed 's|loki|grafana/loki|' \
| sed 's|mailu|ghcr.io/mailu/admin|' \
| sed -r 's|(^nextcloud.+)|\1-apache|' \
| sed 's|node_exporter:|prom/node-exporter:v|' \
| sed 's|^odoo|registry.gitlab.com/librecoop/odoo|' \
| sed 's|onlyoffice|onlyoffice/documentserver|' \
| sed 's|ql_odoo||;s|ql_nextcloud||' | sed -r 's|(postgres.+)|\1-alpine|' \
| sed 's|prometheus:|prom/prometheus:v|' \
| sed -r 's|(redis.+)|\1-alpine|' \
| sed 's|seafile:|seafileltd/seafile-mc:|' \
| sed 's|seafile_notification_server|seafileltd/notification-server|' \
| sed 's|seafile_pro|seafileltd/seafile-pro-mc|' \
| sed 's|seasearch|seafileltd/seasearch|' \
| sed -r 's|(tika.+)|apache/\1-full|' \
| sed 's|traefik:|traefik:v|' \
| sed 's|vaultwarden|vaultwarden/server|')

# wordpress, mariadb and memcached don't need to be
# replaced because the images are called the same

for image in $images; do
  echo -n "$image "
  docker manifest inspect "$image" >/dev/null && echo Ok!
done
