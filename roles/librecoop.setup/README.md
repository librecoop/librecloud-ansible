Setup Role
===================

This roles sets up a server and install needed packages to deploy librecloud:

Requirements
------------

Debian-based distro (apt as package manager)

Role Variables
--------------

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: librecoop.setup
```

License
-------

GPLv3

Author Information
------------------

librecoop https://www.librecoop.es
