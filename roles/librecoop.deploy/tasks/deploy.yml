# Input argument
#
# Each element in the docker_compose_workload list can contain the following keys:
#
# - name: Name of the workload
#
# - files: Dictionary of files to be processed
#
# - files.root: The root of the files, if defined it will be copied entirely
#
# - files.templates: List of template files to be processed. They are searched inside files.root if it is defined, and in the default templates directory otherwise
#
# - environment: Dictionary of ENV variables
#
# - repo: If provided, git repository to get the docker-compose.yml from (incompatible with `templates`
#
# - version : If provided, git branch to get checkout (incompatible with `templates`
#
# - restart: If set to True, force a restar the docker-compose even if there are no changes.
#
# - remove_volumes: If set to True, remove all volumes before restarting the compose (implies `restart`)

- name: Deploy
  no_log: False
  block:

  - name: Git clone
    git:
      repo: "{{ service.repo }}"
      dest: "{{ workload_src_path }}/{{ project }}"
      version: "{{ service.version | default('main') }}"
      accept_hostkey: true
    when: service.repo | default(false)

  - name: Template
    block:
      - name: Empty directory
        file:
          path: "{{ workload_src_path }}/{{ project }}"
          state: absent

      - name: Create directory
        file:
          path: "{{ workload_src_path }}/{{ project }}"
          state: directory

      - name: Copy files
        synchronize:
          src: "{{ service.files.root }}/{{ service_name }}/"
          dest: "{{ workload_src_path }}/{{ project }}/"
        when: service.files.root | default(false)

      - name: Template files
        template:
          src: "{{ service.files.root | default('.') }}/{{ service_name }}/{{ template_file.name }}"
          dest: "{{ workload_src_path }}/{{ project }}/{{ template_file.name }}"
          mode: "{{ template_file.mode | default('0644') }}"
          owner: "{{ template_file.owner | default(omit) }}"
          group: "{{ template_file.group | default(omit) }}"
        loop: "{{ service.files.templates }}"
        loop_control:
          loop_var: template_file
        when: service.files.templates | default(false)

    when: service.files | default(false)

  - name: Tear down
    community.docker.docker_compose_v2:
      project_src: "{{ workload_src_path }}/{{ project }}/{{ service.subdir | default() }}"
      remove_volumes: "{{service.remove_volumes}}"
      state: absent
    environment: "{{ service.environment | default({}) }}"
    when: service.restart | default(false) or service.remove_volumes | default(false)
    register: teardown
    until: not teardown.changed
    retries: 5
    delay: 1

  - name: Up
    community.docker.docker_compose_v2:
      project_src: "{{ workload_src_path }}/{{ project }}/{{ service.subdir | default() }}"
      project_name: "{{ project }}"
      pull: always
      remove_orphans: true
    environment: "{{ service.environment | default({}) }}"

  when: service | default(false) # don't do anything if the service definition doesn't exist
