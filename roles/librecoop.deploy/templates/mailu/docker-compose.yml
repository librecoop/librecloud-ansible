services:

  # External dependencies
  redis:
    image: redis:{{ redis_image_version }}-alpine
    restart: always
    volumes:
      - "mailu-redis:/data"
    depends_on:
      - resolver
    dns:
      - 192.168.203.254

{% if not development %}
  certdumper:
    restart: always
    image: ghcr.io/mailu/traefik-certdumper:{{ mailu_image_version }}
    environment:
      - DOMAIN={{ mailu_host }}
      - TRAEFIK_VERSION=v2
    volumes:
      - {{ workload_src_path }}/acme:/traefik
      - mailu-certs:/output
{% endif %}

  # Core services
  front:
    image: ghcr.io/mailu/nginx:{{ mailu_image_version }}
    restart: always
    env_file: mailu.env
    environment:
      - SECRET_KEY=${SECRET_KEY}
    networks:
      - default
      - webmail
{% if 'webdav' in mailu_enable %}
      - radicale
{% endif %}
    volumes:
      - "mailu-certs:/certs"
    depends_on:
      - resolver
    dns:
      - 192.168.203.254
    labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.mailu-https.redirectscheme.scheme=https"

      - "traefik.http.routers.mailu-http.entrypoints=web"
      - "traefik.http.routers.mailu-http.rule=Host(`{{ mailu_host }}`) || Host(`autoconfig.{{ mailu_domain }}`)"
      - "traefik.http.routers.mailu-http.middlewares=mailu-https@docker"

      - "traefik.http.routers.mailu.entrypoints=websecure"
      - "traefik.http.routers.mailu.rule=Host(`{{ mailu_host }}`) || Host(`autoconfig.{{ mailu_domain }}`)"

      - "traefik.http.routers.mailu.tls=true"
      - "traefik.http.routers.mailu.tls.certresolver=default"
      - "traefik.http.services.mailu.loadbalancer.server.port=80"

      - "traefik.tcp.routers.mailu-smtp.entrypoints=smtp"
      - "traefik.tcp.routers.mailu-smtp.rule=HostSNI(`*`)"
      - "traefik.tcp.routers.mailu-smtp.service=mailu-smtp"
      - "traefik.tcp.services.mailu-smtp.loadbalancer.proxyprotocol.version=2"
      - "traefik.tcp.services.mailu-smtp.loadbalancer.server.port=25"

{% if development %}
      - "traefik.tcp.routers.mailu-pop3.entrypoints=pop3"
      - "traefik.tcp.routers.mailu-pop3.rule=HostSNI(`*`)"
      - "traefik.tcp.routers.mailu-pop3.service=mailu-pop3"
      - "traefik.tcp.services.mailu-pop3.loadbalancer.proxyprotocol.version=2"
      - "traefik.tcp.services.mailu-pop3.loadbalancer.server.port=110"

      - "traefik.tcp.routers.mailu-imap.entrypoints=imap"
      - "traefik.tcp.routers.mailu-imap.rule=HostSNI(`*`)"
      - "traefik.tcp.routers.mailu-imap.service=mailu-imap"
      - "traefik.tcp.services.mailu-imap.loadbalancer.proxyprotocol.version=2"
      - "traefik.tcp.services.mailu-imap.loadbalancer.server.port=143"
{% endif %}

      - "traefik.tcp.routers.mailu-smtps.entrypoints=smtps"
      - "traefik.tcp.routers.mailu-smtps.rule=HostSNI(`*`)"
      - "traefik.tcp.routers.mailu-smtps.service=mailu-smtps"
      - "traefik.tcp.services.mailu-smtps.loadbalancer.proxyprotocol.version=2"
      - "traefik.tcp.services.mailu-smtps.loadbalancer.server.port=465"

      - "traefik.tcp.routers.mailu-imaps.entrypoints=imaps"
      - "traefik.tcp.routers.mailu-imaps.rule=HostSNI(`*`)"
      - "traefik.tcp.routers.mailu-imaps.service=mailu-imaps"
      - "traefik.tcp.services.mailu-imaps.loadbalancer.proxyprotocol.version=2"
      - "traefik.tcp.services.mailu-imaps.loadbalancer.server.port=993"

      - "traefik.tcp.routers.mailu-pop3s.entrypoints=pop3s"
      - "traefik.tcp.routers.mailu-pop3s.rule=HostSNI(`*`)"
      - "traefik.tcp.routers.mailu-pop3s.service=mailu-pop3s"
      - "traefik.tcp.services.mailu-pop3s.loadbalancer.proxyprotocol.version=2"
      - "traefik.tcp.services.mailu-pop3s.loadbalancer.server.port=995"

      - "traefik.tcp.routers.mailu-sieve.entrypoints=sieve"
      - "traefik.tcp.routers.mailu-sieve.rule=HostSNI(`*`)"
      - "traefik.tcp.routers.mailu-sieve.service=mailu-sieve"
      - "traefik.tcp.services.mailu-sieve.loadbalancer.proxyProtocol.version=2"
      - "traefik.tcp.services.mailu-sieve.loadbalancer.server.port=4190"

  resolver:
    image: ghcr.io/mailu/unbound:{{ mailu_image_version }}
    env_file: mailu.env
    environment:
      - SECRET_KEY=${SECRET_KEY}
    restart: always
    networks:
      default:
        ipv4_address: 192.168.203.254

  admin:
    image: ghcr.io/mailu/admin:{{ mailu_image_version }}
    restart: always
    env_file: mailu.env
    environment:
      - INITIAL_ADMIN_PW=${INITIAL_ADMIN_PW}
      - SECRET_KEY=${SECRET_KEY}
    volumes:
      - "mailu-data:/data"
      - "mailu-dkim:/dkim"
    depends_on:
      - redis
      - resolver
    dns:
      - 192.168.203.254
    labels:
      backupbot.backup: "true"
      backupbot.backup.volumes: "true"

  imap:
    image: ghcr.io/mailu/dovecot:{{ mailu_image_version }}
    restart: always
    env_file: mailu.env
    environment:
      - SECRET_KEY=${SECRET_KEY}
    volumes:
      - "mailu-mail:/mail"
    networks:
      - default
{% if 'tika' in mailu_enable %}
      - fts_attachments
{% endif %}
    depends_on:
      - front
{% if 'tika' in mailu_enable %}
      - fts_attachments
{% endif %}
      - resolver
    dns:
      - 192.168.203.254
    labels:
      backupbot.backup: "true"
      backupbot.backup.volumes: "true"

  smtp:
    image: ghcr.io/mailu/postfix:{{ mailu_image_version }}
    restart: always
    env_file: mailu.env
    environment:
      - SECRET_KEY=${SECRET_KEY}
    volumes:
      - "mailu-mailqueue:/queue"
    depends_on:
      - front
      - resolver
    dns:
      - 192.168.203.254

{% if 'oletools' in mailu_enable %}
  oletools:
    image: ghcr.io/mailu/oletools:{{ mailu_image_version }}
    hostname: oletools
    restart: always
    networks:
      - oletools
    depends_on:
      - resolver
    dns:
      - 192.168.203.254
{% endif %}

{% if 'tika' in mailu_enable %}
  fts_attachments:
    image: apache/tika:{{ tika_image_version }}-full
    hostname: tika
    restart: always
    deploy:
      resources:
          limits:
            memory: 100M
    networks:
      - fts_attachments
    depends_on:
      - resolver
    dns:
      - 192.168.203.254
    healthcheck:
      test: ["CMD-SHELL", "wget -nv -t1 -O /dev/null http://127.0.0.1:9998/tika || exit 1"]
      interval: 10s
      timeout: 5s
      retries: 3
      start_period: 10s
{% endif %}

  antispam:
    image: ghcr.io/mailu/rspamd:{{ mailu_image_version }}
    hostname: antispam
    restart: always
    env_file: mailu.env
    environment:
      - SECRET_KEY=${SECRET_KEY}
    networks:
      - default
{% if 'oletools' in mailu_enable %}
      - oletools
{% endif %}
{% if 'clamav' in mailu_enable %}
      - clamav
{% endif %}
    volumes:
      - "mailu-filter:/var/lib/rspamd"
    depends_on:
      - front
      - redis
{% if 'oletools' in mailu_enable %}
      - oletools
{% endif %}
{% if 'clamav' in mailu_enable %}
      - antivirus
{% endif %}
      - resolver
    dns:
      - 192.168.203.254
    labels:
      backupbot.backup: "true"
      backupbot.backup.volumes: "true"

  # Optional services

{% if 'clamav' in mailu_enable %}
  antivirus:
    image: clamav/clamav-debian:{{ clamav_image_version }}
    restart: always
    networks:
      - clamav
    volumes:
      - "mailu-clamav:/var/lib/clamav"
    healthcheck:
      test: ["CMD-SHELL", "kill -0 `cat /tmp/clamd.pid` && kill -0 `cat /tmp/freshclam.pid`"]
      interval: 10s
      timeout: 5s
      retries: 3
      start_period: 10s
{% endif %}

{% if 'webdav' in mailu_enable %}
  webdav:
    image: ghcr.io/mailu/radicale:{{ mailu_image_version }}
    restart: always
    volumes:
      - "mailu-dav:/data"
    networks:
      - radicale
    labels:
      backupbot.backup: "true"
      backupbot.backup.volumes: "true"
{% endif %}

{% if 'fetchmail' in mailu_enable %}
  fetchmail:
    image: ghcr.io/mailu/fetchmail:{{ mailu_image_version }}
    restart: always
    env_file: mailu.env
    environment:
      - SECRET_KEY=${SECRET_KEY}
    volumes:
      - "mailu-data-fetchmail:/data"
    depends_on:
      - admin
      - smtp
      - imap
      - resolver
    dns:
      - 192.168.203.254
    labels:
      backupbot.backup: "true"
      backupbot.backup.volumes: "true"
{% endif %}

  # Webmail
  webmail:
    image: ghcr.io/mailu/webmail:{{ mailu_image_version }}
    restart: always
    env_file: mailu.env
    environment:
      - SECRET_KEY=${SECRET_KEY}
    volumes:
      - "mailu-webmail:/data"
    networks:
      - webmail
    depends_on:
      - front
    labels:
      backupbot.backup: "true"
      backupbot.backup.volumes: "true"

volumes:
  mailu-redis:
  mailu-certs:
  mailu-data:
  mailu-dkim:
  mailu-mail:
  mailu-mailqueue:
  mailu-filter:
{% if 'clamav' in mailu_enable %}
  mailu-clamav:
{% endif %}
{% if 'webdav' in mailu_enable %}
  mailu-dav:
{% endif %}
{% if 'fetchmail' in mailu_enable %}
  mailu-data-fetchmail:
{% endif %}
  mailu-webmail:

networks:
  default:
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: 192.168.203.0/24
{% if 'webdav' in mailu_enable %}
  radicale:
    driver: bridge
{% endif %}
  webmail:
    driver: bridge
{% if 'clamav' in mailu_enable %}
  clamav:
    driver: bridge
{% endif %}
{% if 'oletools' in mailu_enable %}
  oletools:
    driver: bridge
    internal: true
{% endif %}
{% if 'tika' in mailu_enable %}
  fts_attachments:
    driver: bridge
    internal: true
{% endif %}
