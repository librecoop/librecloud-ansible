#!/bin/bash

{% if (variables.requirements | default(false) and ((variables.version | default(odoo_image_version)) in ['13.0', '14.0'])) %}
python3 -m pip install -U pip
python3 -m pip install -r requirements.txt
{%- elif variables.requirements | default(false) -%}
python3 -m pip install -U pip --break-system-packages
python3 -m pip install -r requirements.txt --break-system-packages
{% endif %}

source entrypoint.sh odoo
