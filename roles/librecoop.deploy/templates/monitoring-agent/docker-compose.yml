services:
  node-exporter:
    image: prom/node-exporter:v{{ node_exporter_image_version }}
    restart: always
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command:
      - '--path.procfs=/host/proc'
      - '--path.rootfs=/rootfs'
      - '--path.sysfs=/host/sys'
      - '--collector.filesystem.mount-points-exclude=^/(sys|proc|dev|host|etc)($$|/)'
{% if 'grafana' not in deploy | map(attribute='service') %}
    labels:
      traefik.enable: "true"

      traefik.http.middlewares.nodeexporter-https.redirectscheme.scheme: "https"
      traefik.http.middlewares.nodeexporter-auth.basicauth.users: "admin:${MONITORING_PASSWORD_HASHED}"

      traefik.http.routers.nodeexporter-http.entrypoints: "web"
      traefik.http.routers.nodeexporter-http.rule: "Host(`{{ node_exporter_host }}`)"
{% if not development %}
      traefik.http.routers.nodeexporter-http.middlewares: "nodeexporter-https@docker"
{% endif %}
      traefik.http.routers.nodeexporter.entrypoints: "websecure"
      traefik.http.routers.nodeexporter.rule: "Host(`{{ node_exporter_host }}`)"
      traefik.http.routers.nodeexporter.middlewares: "nodeexporter-auth@docker"

      traefik.http.routers.nodeexporter.tls: "true"
      traefik.http.routers.nodeexporter.tls.certresolver: "default"
{% endif %}
{% if 'grafana' in deploy | map(attribute='service') %}
    networks:
      - monitoring
{% endif %}

  cadvisor:
    image: gcr.io/cadvisor/cadvisor:v{{ cadvisor_image_version }}
    restart: always
    command:
      - "--housekeeping_interval=30s"
      - "--docker_only=true"
      - "--disable_metrics=percpu,sched,tcp,udp,disk,diskIO,hugetlb,referenced_memory,cpu_topology,resctrl"
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
{% if 'grafana' not in deploy | map(attribute='service') %}
    labels:
      traefik.enable: "true"

      traefik.http.middlewares.cadvisor-https.redirectscheme.scheme: "https"
      traefik.http.middlewares.cadvisor-auth.basicauth.users: "admin:${MONITORING_PASSWORD_HASHED}"

      traefik.http.routers.cadvisor-http.entrypoints: "web"
      traefik.http.routers.cadvisor-http.rule: "Host(`{{ cadvisor_host }}`)"
{% if not development%}
      traefik.http.routers.cadvisor-http.middlewares: "cadvisor-https@docker"
{% endif %}
      traefik.http.routers.cadvisor.entrypoints: "websecure"
      traefik.http.routers.cadvisor.rule: "Host(`{{ cadvisor_host }}`)"
      traefik.http.routers.cadvisor.middlewares: "cadvisor-auth@docker"

      traefik.http.routers.cadvisor.tls: "true"
      traefik.http.routers.cadvisor.tls.certresolver: "default"
{% endif %}
    depends_on:
      - redis
{% if 'grafana' in deploy | map(attribute='service') %}
    networks:
      - monitoring
{% endif %}
  redis:
    image: redis:{{ redis_image_version }}-alpine
    restart: always
{% if 'grafana' in deploy | map(attribute='service') %}
    networks:
      - monitoring
{% endif %}

  promtail:
    image: grafana/promtail:{{ loki_image_version }}
    restart: always
    volumes:
      - /var/lib/docker/containers:/var/lib/docker/containers
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/run/promtail:/var/run/promtail
      - /var/log:/var/log
      - /var/log/journal:/var/log/journal
      - /run/log/journal:/run/log/journal/
      - /etc/machine-id:/etc/machine-id
      - ./promtail-config.yaml:/etc/promtail/promtail-config.yaml
    command: -config.file=/etc/promtail/promtail-config.yaml

{% if 'grafana' in deploy | map(attribute='service') %}
networks:
  monitoring:
    name: "grafana_monitoring"
    external: true
{% endif %}
